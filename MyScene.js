import React, { Component } from 'react';
import { AppRegistry, Text, View, Image, StyleSheet } from 'react-native';
import TabNavigator from "react-native-tab-navigator";

const [tabTag, tabNames, normalIcons, selectedIcons] = [
    ["tab1", "tab2", "tab3", "tab4"],
    ["首页", "行家", "消息", "我"],
    [require('./images/tabs/icon_1.png'), require('./images/tabs/icon_2.png'), require('./images/tabs/icon_3.png'), require('./images/tabs/icon_4.png')],
    [require('./images/tabs/icon_1s.png'), require('./images/tabs/icon_2s.png'), require('./images/tabs/icon_3s.png'), require('./images/tabs/icon_4s.png')]
];
export default class MyScene extends Component {
    state = {
        selectedTab: tabTag[0]
    };

    render() {
        let aarItem = [];
        for (let i = 0; i < 4; i++) {
            let tag = tabTag[i];
            aarItem.push(
                <TabNavigator.Item
                    key={i}
                    selected={this.state.selectedTab === tag}
                    title={tabNames[i]}
                    selectedTitleStyle={{color: "#3496f0"}}
                    renderIcon={() => <Image style={styles.tabIcon} source={normalIcons[i]} color="#666"/>}
                    renderSelectedIcon={() => <Image style={styles.tabIcon} source={selectedIcons[i]} color="#3496f0"/>}
                    onPress={() => this.setState({selectedTab: tag})}>
                    <Text>tab {i + 1}</Text>
                </TabNavigator.Item>
            )
        }
        return (
            <TabNavigator style={styles.container}>
                {
                    aarItem
                }
            </TabNavigator>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    tabIcon: {
        width: 30,
        height: 30,
        resizeMode: 'stretch',
        marginTop: 12.5
    }
});
