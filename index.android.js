/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from "react";
import {AppRegistry, Text, View, Image, StyleSheet} from "react-native";
import MyScene from "./MyScene";

class TabDemo extends Component {
    render() {
        return (
            <MyScene />
        );
    }
}

AppRegistry.registerComponent('ReactNativeDemo', () => TabDemo);