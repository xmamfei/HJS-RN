/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View
} from 'react-native';
import MyScene from "./MyScene";

export default class ReactNativeDemo extends Component {
  render() {
    return (
      <MyScene />
    );
  }
}
AppRegistry.registerComponent('ReactNativeDemo', () => ReactNativeDemo);
